# Geogebra5

Sources: https://github.com/geogebra

Free mathematics software for learning and teaching
 GeoGebra is free dynamic mathematics software for all levels of education
 that joins geometry, algebra, graphing, and calculus in one
 easy-to-use package.
 .
 Quick Facts:
 Graphics, algebra and spreadsheet are connected and fully dynamic.
 Easy-to-use interface, yet many powerful features.
 Authoring tool to create and share interactive online learning materials.
 Available in many languages for our millions of users around the world.
 Free and open source math/maths software.